local paths = {}

paths.install_prefix = [[/home/blp/Downloads/Human_Level_Control_through_Deep_Reinforcement_Learning/torch]]
paths.install_bin = [[/home/blp/Downloads/Human_Level_Control_through_Deep_Reinforcement_Learning/torch/bin]]
paths.install_man = [[/home/blp/Downloads/Human_Level_Control_through_Deep_Reinforcement_Learning/torch/share/man]]
paths.install_lib = [[/home/blp/Downloads/Human_Level_Control_through_Deep_Reinforcement_Learning/torch/lib]]
paths.install_share = [[/home/blp/Downloads/Human_Level_Control_through_Deep_Reinforcement_Learning/torch/share]]
paths.install_include = [[/home/blp/Downloads/Human_Level_Control_through_Deep_Reinforcement_Learning/torch/include]]
paths.install_cmake = [[/home/blp/Downloads/Human_Level_Control_through_Deep_Reinforcement_Learning/torch/share/cmake/torch]]

return paths
