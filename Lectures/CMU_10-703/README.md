# CMU RESOURCES
10-703[Deep Reinforcement Learning and Control](https://www.andrew.cmu.edu/course/10-703/)

10-701[Tom Mitchell Class on Machine Learning](https://www.youtube.com/playlist?list=PLl-BBnDxtUt1hLXmIwu27P22bTi6VwMkN)

CMU 10-702 Statistical Machine Learning [videos](https://www.youtube.com/playlist?list=PLTB9VQq8WiaCBK2XrtYn5t9uuPdsNm7YE) [slides](http://www.stat.cmu.edu/~larry/=sml/)

[Geoff Gordon](https://www.youtube.com/user/professorgeoff)

[11-785](https://www.youtube.com/channel/UC8hYZGEkI2dDO8scT8C5UQA/playlists)









