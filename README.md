Resources for DRL:

1. Stanford CS234 [lecture](https://www.youtube.com/playlist?list=PLoROMvodv4rOSOPzutgyCTapiGlY2Nd8u)
2. DeepMind David Silver [slides](http://www0.cs.ucl.ac.uk/staff/d.silver/web/Teaching.html)[lecture](https://www.youtube.com/playlist?list=PLqYmG7hTraZDM-OYHWgPebj2MfCFzFObQ)
3. [Berkeley Stat157](https://www.youtube.com/playlist?list=PLZSO_6-bSqHQHBCoGaObUljoXAyyqhpFW)
4. [University of Waterloo CS885](https://cs.uwaterloo.ca/~ppoupart/teaching/cs885-spring18/) 
5. [CMU 10-703](https://github.com/BaiLiping/BLP/tree/master/DRL/CMU10703)
6. [Berkeley RL Bootcamp2017](https://sites.google.com/view/deep-rl-bootcamp/lectures)
7. [OpenAI Baseline Implementation of Algs](https://github.com/openai/baselines)
8. [MorvanZhou Implementation of Algs](https://github.com/MorvanZhou/Reinforcement-learning-with-tensorflow)
9. Workshop on Learning for Control (57th IEEE Conference on Decision and Control, aka CDC) [link](https://kgatsis.github.io/learning_for_control_workshop_CDC2018/)
10. Learning for Dynamics and Control (L4DC) [link](https://l4dc.mit.edu/) [videos](https://www.youtube.com/playlist?list=PLYx2nCJDi_QFrGOmIM0ale8T_1Fqu8OIF)
11. [Deep Learning](http://deeplearning.cs.cmu.edu/)
12. [CS230 Deep Learning with AN](http://cs230.stanford.edu/)
13. [Statistical Learning](https://www.youtube.com/channel/UCGoxKRfTs0jQP52cfHCyyRQ)
14. [CS229 Machine Learning](http://cs229.stanford.edu/syllabus.html)
15. [Advanced Survey of Reinforcement Learning](http://web.stanford.edu/class/archive/cs/cs332/cs332.1182/#!syllabus.md)
16. CS285 Fall 2019[webiste](http://rail.eecs.berkeley.edu/deeprlcourse)  [discussio ](https://www.reddit.com/r/berkeleydeeprlcourse/)  [videos](https://www.youtube.com/playlist?list=PLkFD6_40KJIwhWJpGazJ9VSj9CFMkb79A)

Books for DRL:

1. [Statistical Learning](https://github.com/BaiLiping/BLP/blob/master/DRL/Books/Statistical%20Learning.pdf) quite a lot stats concepts made its way into DRL, such as importance sampling, KL divergence, Heffoling Inequalities etc. It is a good background
2. [RL by Rich Sutton](https://github.com/BaiLiping/BLP/blob/master/DRL/Books/RLbook2018.pdf) Approximation with NN is a new development, but the overall structure of RL is still what Rich Sutton lays out in his book: Dynamic Programming->Monto Carlo->TD(0)->TD(lambda))->Tabular Methods->Q-learning->Generalization with Function Approximations->Policy Gradient->Actor-Critic with Advantage, also the distinction between on-policy learning, off-policy learning
3. [Decision Making Under Uncertainty](https://web.stanford.edu/class/aa228/cgi-bin/wp/)
4. [Algorithms for Reinforcement Learning](https://sites.ualberta.ca/~szepesva/RLBook.html)

Survey:
1. [List of Implementation and Papers of PG Algorithms](https://lilianweng.github.io/lil-log/2018/04/08/policy-gradient-algorithms.html#actor-critic)
2. [A Tour of Reinforcement Learning: The View from Continuous Control](https://github.com/BaiLiping/BLP/blob/master/DRL/Books/1806.09460.pdf)
3. [seeing theory](https://seeing-theory.brown.edu)

Articles:
1. [Distill](https://distill.pub/)
2. [AI Note from deeplearning.ai](https://www.deeplearning.ai/ai-notes/)
3. [NIPS papers on Reinforcement Learning](https://papers.nips.cc/search/?q=reinforcement+learning)

